<?php
//Registro de todos los servicios y configuración de nuestra aplicación
use Silex\Application;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use User\UserProvider;
use  auntentificacion\DefaultAuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;

$app = new Application();
//$app['debug'] = true;
$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__.'/views'),
));
$app->before(function ($request) {
    $request->getSession()->start();
 });
//password = foo
$app->register(new SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            //rutas protegidas por el acceso
            'pattern' => '(^/admin)|(^/chart)|(^/verservidores)|(^/agregarservidores)|(^/eliminarservidores)
              |(^/verregistros)|(^/vernotificaciones)|(^/confnotificaciones)',
            //'form' => array('login_path' => '/login', 'check_path' => '/admin/login_check'),
            'anonymous' => false,
            'form' => array(
                'login_path' =>'/login',
                "check_path" => '/admin/login_check',
                'username_parameter'=> '_username',
                'password_parameter'=> '_password',
            ),
            'logout' => array('logout_path' => '/admin/logout'),
            'users' =>function () use ($app) {
                return new  UserProvider($app['db']);
            },
        ),
    ),
    'security.access_rules' => array(
        array('^/chart','ROLE_ADMIN'),
        array('(^/vernotificaciones)','ROLE_ADMIN'),
        array('(^/confnotificaciones)','ROLE_ADMIN'),
        array('^/verservidores','ROLE_ADMIN'),
        array('^/agregarservidores','ROLE_ADMIN'),
        array('^/verregistros','ROLE_ADMIN'),
        array('^/eliminarservidores','ROLE_ADMIN'),
        array('^/admin','ROLE_ADMIN'),

    )
));
/*
$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'    => 'pdo_mysql',
        'host'      => '172.17.0.245',
        'dbname'    => 'users',
        'user'      => 'clusterelastic@172.17.0.245',
        'password'  => 'Camaleon',
        'charset'   => 'utf8',
    ),
));
*/
//$app->register(new Silex\Provider\LocaleServiceProvider());
$app->register(new SessionServiceProvider());
$app['user.options'] = array(
    'mailer' => array('enabled' => true),
    'userClass' => 'Symfony\Component\Security\Core\User\User',
);
$app['security.authentication.success_handler.admin'] = function () use ($app) {
    $handler = new DefaultAuthenticationSuccessHandler($app);
    $handler->setProviderKey('admin');
    return $handler;
};
$app['security.default_encoder'] = function ($app) {
    return $app['security.encoder.digest'];
};
$app['session.db_options'] = array(
   'db_table' => 'sessions',
   'db_id_col' => 'sess_id',
   'db_data_col' => 'sess_data',
   'db_time_col' => 'sess_time',
);
$app['session.storage.handler'] =(function () use ($app) {
   return new \Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler(
           $app['db']->getWrappedConnection(), $app['session.db_options'], $app['session.storage.options']
   );
});
return $app;
//$app->boot();
