<?php
use Silex\Application as Application;
class Model {
    protected $tabla;
    protected $id_tabla;

    public function __construct(Application $app) {
        $this->app = $app;
        $this->conn = $app['db'];
    }

    public function insert($data) {
        return $this->app['db']->insert($this->tabla, $data);

    }

    public function insert_id($data) {
        try {
            $this->app['db']->insert($this->tabla, $data);
            $id = $this->app['db']->lastInsertId();
            return $id;
        } catch (Exception $ex) {
            var_dump($ex->getMessage());
            var_dump($ex->getTraceAsString());
        }
        return false;
    }

    public function delete($id) {
        if (is_array($id)) {
            return $this->app['db']->delete($this->tabla, $id);
        }
        return $this->app['db']->delete($this->tabla, array($this->id_tabla => $id));
    }

    public function deleteWhere($where_array) {
        return $this->app['db']->delete($this->tabla, $where_array);
    }

    public function update($id, $data) {
        try {
            if (is_array($id)) {
                $this->app['db']->update($this->tabla, $data, $id);
            } else {
                $this->app['db']->update($this->tabla, $data, array($this->id_tabla => $id));
            }
            return TRUE;
        } catch (Exception $e) {
            return FALSE;
        }
    }

    public function updateWhere($id,$data){
      return $this->app['db']->update($this->tabla, $data, $id);
    }

    public function last_id() {
        $id = $this->app["db"]->lastInsertId();
        return $id;
    }

    public function getCount($where = null) {
        if ($where == null) {
            $sql = ("SELECT count(*) as total FROM $this->tabla  ");
        } else {
            $sql = ("SELECT count(*) as total FROM $this->tabla  ");
            if (is_array($where)) {
                $w = $this->parseWhere($where);
                $sql = ("SELECT count(*) as total FROM $this->tabla  where  $w ");
            }
        }
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute();
        $n = $stmt->fetch(PDO::FETCH_OBJ);
        if ($n) {
            return $n->total;
        }
        return 0;
    }

    public function getAll($colums = "*", $order_by = "") {
        $sql = ("SELECT $colums FROM $this->tabla $order_by ");
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute();
        $n = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $n;
    }

    protected function query($sql, $params = array()) {
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute($params);
        $n = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $n;
    }

    protected function execute($sql, $params = array()) {
        $stmt = $this->app['db']->prepare($sql);
        $n = $stmt->execute($params);
        return $n;
    }

    public function getList($pagina, $colums = "*") {
        $sql = ("SELECT $colums FROM $this->tabla " . $this->limit($pagina));
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute();
        $n = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $n;
    }

    public function getListWhere($where, $colums = '*', $order_by = '', $order = 'asc') {
        $w = $this->parseWhere($where);
        $order = $order_by == "" ? "" : $order;
        $order_by = $order_by == "" ? "" : "order by $order_by";
        $sql = ("SELECT $colums FROM $this->tabla  where $w $order_by $order");
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute();
        $n = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $n;
    }

    public function getListWhereLimit($where, $colums = "*", $order_by = "", $order = "asc", $limit = "") {
        $w = $this->parseWhere($where);
        $order = $order_by == "" ? "" : $order;
        $order_by = $order_by == "" ? "" : "ORDER BY $order_by";
        $sql = ("SELECT $colums FROM $this->tabla  WHERE $w $order_by $order $limit");
        $stmt = $this->app['db']->prepare($sql);
        $stmt->execute();
        $n = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $n;
    }

    public function get($id, $colums = "*") {
        if (is_array($id)) {
            $c = 0;
            $where = "";
            $count = count($id) - 1;
            foreach ($id as $key => $value) {
                if ($count == $c) {
                    $where .= " $key='$value' ";
                } else {
                    $where .= " $key='$value' and";
                }
                $c++;
            }
            $sql = "SELECT $colums FROM $this->tabla where $where limit 1 ";
            $stmt = $this->app['db']->prepare($sql);
            $stmt->execute();
        } else {
            $sql = "SELECT $colums FROM $this->tabla where $this->id_tabla=?  limit 1";
            $stmt = $this->app['db']->prepare($sql);
            $stmt->execute(array($id));
        }
        $n = $stmt->fetch(PDO::FETCH_OBJ);
        return $n;
    }

    private function limit($pag) {
        if (!$pag || $pag == 0) {
            return 'limit 25';
        } else {
            return 'limit ' . ($pag * 25) . ',25';
        }
    }

    public function parseWhere($where) {

        if (is_array($where)) {
            $w = '';
            $c = 0;
            $count = count($where) - 1;
            foreach ($where as $key => $value) {
                if ($count == $c) {
                    $w .= " $key= '". str_replace("'",'`',$value) . "'  ";
                } else {
                    $w .= " $key='". str_replace("'",'`',$value) . "' and";
                }
                $c++;
            }
          //  var_dump($w);
            return $w;
        }
    }

}
