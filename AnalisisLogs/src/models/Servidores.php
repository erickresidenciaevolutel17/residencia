<?php

use Silex\Application as Application;
use Silex\Provider\Form\SilexFormExtension;
use \Symfony\Component\Security\Acl\Exception\Exception;


class Servidores extends Model {
    public function __construct(\Silex\Application $app) {
        parent::__construct($app);
        $this->tabla = 'servidores';
        $this->id_tabla = 'id_servidor';
    }

    public function mostrar_servidores(){
      $consulta="select * from servidores";

      $result= $this->query($consulta);
      if ($result) {
            return $result;
        }
        return FALSE;
    }
    public function insertar_servidores($array){
      $this->insert($array);
    }
    public function conteo($where=null){
      $result=$this->getCount($where);
      return $result;
    }
    




}
