<?php
namespace Server\ServerControler;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

  class ServerControler{

    public function listar(Request $request , Application $app){
      $servDB=new \Servidores($app);
      $prueba =$servDB->mostrar_servidores();
          return  $app['twig']->render('BackEnd/Vistas/verservidores.twig', array(
             'user'=>$app['session']->get('user'),
             'datos'=>$prueba,
           ));
  }
  public function actualizar(Request $request, Application $app){
    $id =$request->attributes->get('id');
    $servDB=new \Servidores($app);
    $array_consulta= array('id_servidor'=> $id);
      //echo var_dump($array_consulta);
      //exit;
    $consultaid=$servDB->getListWhere($array_consulta);
  //  echo var_dump($consultaid);
    //exit;
    return  $app['twig']->render('BackEnd/Vistas/updateserver.twig', array(
       'user'=>$app['session']->get('user'),
       'nombre_servidor'=>$consultaid[0]->nombre_servidor,
       'nombre_archivo'=>$consultaid[0]->archivo_acceso,
       'tipo'=>$consultaid[0]->tipo,
       'id_servidor'=>$consultaid[0]->id_servidor,
     ));
  }
  public function updateserver(Request $request, Application $app){
      $id =$request->attributes->get('id');

      $servDB=new \Servidores($app);
      $name_ser = $request->get('nombre_servidor');
      $array = array('nombre_servidor' =>$name_ser);
      $consultaid=$servDB->update($id, $array);
      return $app->redirect($app['url_generator']->generate('verservidores'));
  }
  public function agregar(Request $request, Application $app){
    /*
    elementos  a recuperar del formulario
    */
    $servDB=new \Servidores($app);
    $name_ser = $request->get('nombre_servidor');
    $ip_servidor=$request->get('ip_servidor');
    $name_file=$request->get('nombre_archivo');
    $tipo=$request->get('tipo_archivo');
    /*consulta para el conteo si existe el archivo con el mismo nombre y el tipo*/
    $count= $servDB->conteo(array('archivo_acceso'=>$name_file,
      'tipo'=>$tipo
      ));
    /*path de archivos de configuracion*/
    $path_mysql="/Users/evolutel/cluster1/Nodo1/logmysql/logstash-5.5.2/bin/configuracion.conf";
    $pathapache_access="/Users/evolutel/cluster1/Nodo1/logaccesso/logstash-5.5.2/bin/configuracion.conf";
    $pathapache_error="/Users/evolutel/cluster1/Nodo1/logapacheerror/logstash-5.5.2/bin/configuracion.conf";
    $log_propio="/Users/evolutel/cluster1/Nodo1/logpropio/logstash-5.5.2/bin/configuracion.conf";
    $path_tomcat="/Users/evolutel/cluster1/Nodo1/logcatalinaaccess/logstash-5.5.2/bin/configuracion.conf";
    $path_tomcat_out="/Users/evolutel/cluster1/Nodo1/catalinaout/logstash-5.5.2/bin/configuracion.conf";
    $path_prueba="/Users/evolutel/cluster1/desdeaqui.txt";
    /* fin de path de archivos de configuracion de logstash*/
    /*path de logs a leer*/
    $path_logs_a_leer='/Users/evolutel/cluster1/Nodo1/logsaleer/'.$name_file;
    //creamos el archivo del log
    $control = fopen($path_logs_a_leer,"w+");
    /*fin de paths de logs a leer*/
    /*Parametros de insercion en la tabbla de servidores*/
    $parametros=array(
        'nombre_servidor' => $name_ser,
        'archivo_acceso' => $name_file,
        'tipo'=>$tipo,
        'ip_servidor'=>$ip_servidor,
      );
    //  if($count== "0"){
        //redirigiendo a la vista de ver servidores
        if($tipo == "Apache_Access"){
          $pruebas =$servDB->insertar_servidores($parametros);
          $tipo=strtolower($tipo);
          $this->escribir($pathapache_access,$path_logs_a_leer,$tipo);
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        if($tipo == "Apache_error"){
          $pruebas =$servDB->insertar_servidores($parametros);
          $tipo=strtolower($tipo);
          $this->escribir($pathapache_error,$path_logs_a_leer,$tipo);
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        if($tipo == "mysql_error"){
          $pruebas =$servDB->insertar_servidores($parametros);
          $tipo=strtolower($tipo);
          $this->escribir($path_mysql,$path_logs_a_leer,$tipo);
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        if($tipo == "Log propio"){
          $pruebas =$servDB->insertar_servidores($parametros);
          //$tipo=strtolower($tipo);
          $this->escribir($log_propio,$path_logs_a_leer,"logpropio");
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        if($tipo == "Tomcat"){
          $pruebas =$servDB->insertar_servidores($parametros);
          $tipo=strtolower($tipo);
          $this->escribir($path_tomcat,$path_logs_a_leer,"tomcat");
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        if($tipo == "Tomcat Out"){
          $pruebas =$servDB->insertar_servidores($parametros);
          //$tipo=strtolower($tipo);
          $this->escribir_multiline($path_tomcat_out,$path_logs_a_leer,"tomcat_out");
          return $app->redirect($app['url_generator']->generate('verservidores'));
        }
        //}
        //else{
      //echo "ya esta registrado ";
      //echo var_dump($count);
            //exit;
        //    //return 0;
          //}
    }
    public function escribir($path_abrir,$path_escribir,$tipo){
      //$control = fopen($path_abrir,"w+");
      //se carga el archivo
      $lineas = file($path_abrir);
      //palabra a buscar
      $palabra="#linea";
      //contador que me da la posicion de la palabra
      $pos = 0;
      $aux1 = 0; //auxiliar para tener el contador de la posicion de la palabra guia
      //recorremos el archivo/con esto se comprueba la posicion de la cadena que voy a reemplazar
      foreach($lineas as $linea){
      //comparamos linea por linea
      //si existe la cadena,el cotador de aumenta
        if(strstr($linea,$palabra)){
          //echo "si esta la palabra $linea, está en la linea : ".$pos;
            $aux1 = $pos;
        }
          $pos++;
        }
        //cargamos de nuevo el archivo a leer en la variable archivo
        $archivo = $path_abrir;
        $abrir = fopen($archivo,'r+');
        $contenido = fread($abrir,filesize($archivo));
        fclose($abrir);
        //Separar linea por linea
        $contenido = explode("\n",$contenido);
        // Modificar linea deseada ( 2 )
        $stringpp='file{
path =>"'.$path_escribir.'"
type =>"'.$tipo .'"
}
#linea';
        $contenido[$aux1] = $stringpp;
        $contenido = implode("\r\n",$contenido);
        // Guardar Archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
    }
    public function escribir_multiline($path_abrir,$path_escribir,$tipo){
      //$control = fopen($path_abrir,"w+");
      //se carga el archivo
      $lineas = file($path_abrir);
      //palabra a buscar
      $palabra="#linea";
      //contador que me da la posicion de la palabra
      $pos = 0;
      $aux1 = 0; //auxiliar para tener el contador de la posicion de la palabra guia
      //recorremos el archivo/con esto se comprueba la posicion de la cadena que voy a reemplazar
      foreach($lineas as $linea){
      //comparamos linea por linea
      //si existe la cadena,el cotador de aumenta
        if(strstr($linea,$palabra)){
          //echo "si esta la palabra $linea, está en la linea : ".$pos;
            $aux1 = $pos;
        }
          $pos++;
        }
        //cargamos de nuevo el archivo a leer en la variable archivo
        $archivo = $path_abrir;
        $abrir = fopen($archivo,'r+');
        $contenido = fread($abrir,filesize($archivo));
        fclose($abrir);
        //Separar linea por linea
        $contenido = explode("\n",$contenido);
        // Modificar linea deseada ( 2 )
        $stringpp='file{
path =>"'.$path_escribir.'"
type =>"'.$tipo .'"
codec => multiline {
         pattern => "^%{DATA:day} %{DATA:DAY}\, %{DATA:year} %{HOUR}\:%{MINUTE}$
         negate => true
         what => "previous"
    }
     }
#linea';
        $contenido[$aux1] = $stringpp;
        $contenido = implode("\r\n",$contenido);
        // Guardar Archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
    }
    public function escribir_eliminar($path_abrir,$path_escribir){
      //$control = fopen($path_abrir,"w+");
      //se carga el archivo
      $lineas = file($path_abrir);
      //palabra a buscar
      $palabra='path =>"'.$path_escribir.'"';
      //contador que me da la posicion de la palabra
      $pos = 0;
      $aux1 = 0; //auxiliar para tener el contador de la posicion de la palabra guia
      //recorremos el archivo/con esto se comprueba la posicion de la cadena que voy a reemplazar
      foreach($lineas as $linea){
      //comparamos linea por linea
      //si existe la cadena,el cotador de aumenta
        if(strstr($linea,$palabra)){
          //echo "si esta la palabra $linea, está en la linea : ".$pos;
            $aux1 = $pos;
        }
          $pos++;
        }
        //cargamos de nuevo el archivo a leer en la variable archivo
        $archivo = $path_abrir;
        $abrir = fopen($archivo,'r+');
        $contenido = fread($abrir,filesize($archivo));
        fclose($abrir);
        //Separar linea por linea
        $contenido = explode("\n",$contenido);
        // Modificar linea deseada ( 2 )
        $stringpp=' ';
        $contenido[$aux1-1]=$stringpp;
        $contenido[$aux1] = $stringpp;
        $contenido[$aux1+1]=$stringpp;
        $contenido[$aux1+2]=$stringpp;
        $contenido = implode("\r\n",$contenido);
        // Guardar Archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
    }
    public function escribir_eliminar_multifilter($path_abrir,$path_escribir){
      //$control = fopen($path_abrir,"w+");
      //se carga el archivo
      $lineas = file($path_abrir);
      //palabra a buscar
      $palabra='path =>"'.$path_escribir.'"';
      //contador que me da la posicion de la palabra
      $pos = 0;
      $aux1 = 0; //auxiliar para tener el contador de la posicion de la palabra guia
      //recorremos el archivo/con esto se comprueba la posicion de la cadena que voy a reemplazar
      foreach($lineas as $linea){
      //comparamos linea por linea
      //si existe la cadena,el cotador de aumenta
        if(strstr($linea,$palabra)){
          //echo "si esta la palabra $linea, está en la linea : ".$pos;
            $aux1 = $pos;
        }
          $pos++;
        }
        //cargamos de nuevo el archivo a leer en la variable archivo
        $archivo = $path_abrir;
        $abrir = fopen($archivo,'r+');
        $contenido = fread($abrir,filesize($archivo));
        fclose($abrir);
        //Separar linea por linea
        $contenido = explode("\n",$contenido);
        // Modificar linea deseada ( 2 )
        $stringpp=' ';
        $contenido[$aux1-1]=$stringpp;
        $contenido[$aux1] = $stringpp;
        $contenido[$aux1+1]=$stringpp;
        $contenido[$aux1+2]=$stringpp;
        $contenido[$aux1+3]=$stringpp;
        $contenido[$aux1+4]=$stringpp;
        $contenido[$aux1+5]=$stringpp;
        $contenido[$aux1+6]=$stringpp;
        $contenido[$aux1+7]=$stringpp;
        $contenido = implode("\r\n",$contenido);
        // Guardar Archivo
        $abrir = fopen($archivo,'w');
        fwrite($abrir,$contenido);
        fclose($abrir);
    }



    public function eliminar(Request $request, Application $app){
      //paths de los arhcivos de configuracion de logstash
      $path_mysql="/Users/evolutel/cluster1/Nodo1/logmysql/logstash-5.5.2/bin/configuracion.conf";
      $pathapache_access="/Users/evolutel/cluster1/Nodo1/logaccesso/logstash-5.5.2/bin/configuracion.conf";
      $pathapache_error="/Users/evolutel/cluster1/Nodo1/logapacheerror/logstash-5.5.2/bin/configuracion.conf";
      $log_propio="/Users/evolutel/cluster1/Nodo1/logpropio/logstash-5.5.2/bin/configuracion.conf";
      $path_tomcat="/Users/evolutel/cluster1/Nodo1/logcatalinaaccess/logstash-5.5.2/bin/configuracion.conf";
      $path_tomcat_out="/Users/evolutel/cluster1/Nodo1/catalinaout/logstash-5.5.2/bin/configuracion.conf";



      $id =$request->attributes->get('id');

      $servDB=new \Servidores($app);
      $array_consulta= array('id_servidor'=> $id);
        //echo var_dump($array_consulta);
        //exit;

      $consultaid=$servDB->getListWhere($array_consulta);
      $path_logs_a_leer='/Users/evolutel/cluster1/Nodo1/logsaleer/'.$consultaid[0]->archivo_acceso;

      if(($consultaid[0]->tipo)== "Apache_Access"){

          $this->escribir_eliminar($pathapache_access,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "Apache_error"){
          $this->escribir_eliminar($pathapache_error,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "mysql_error"){
          $this->escribir_eliminar($path_mysql,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "Log propio"){
          $this->escribir_eliminar($log_propio,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "Log propio"){
          $this->escribir_eliminar($log_propio,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "Tomcat"){
          $this->escribir_eliminar($path_tomcat,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }
      if(($consultaid[0]->tipo)== "Tomcat Out"){
          $this->escribir_eliminar_multifilter($path_tomcat_out,$path_logs_a_leer);
          $prueba =$servDB->delete($id);
          return $app->redirect($app['url_generator']->generate('verservidores'));

      }


    }


}
