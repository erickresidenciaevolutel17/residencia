<?php
//controladores para la aplicacion
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Schema\Table;
use Symfony\Component\Security\Core\Encoder\PlaintextPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Elasticsearch\ClientBuilder;
use Server\serverControler;
// Lógica de nuestra aplicación

$app->get('/', function () use ($app) {
   return  $app['twig']->render('FrontEnd/portada.twig');
})->bind('portada');


//texto plano para contraseña

$app['security.default_encoder'] = function ($app) {
    // Plain text (e.g. for debugging)
    return new MessageDigestPasswordEncoder('sha1',false,1);
};
    /*
      Inicio de vistas con funciones anonimas
    */
      //vista de login
$app->get('/login', function (Request $request) use ($app) {

    return $app['twig']->render('BackEnd/login2.twig', array(
        'error'         => $app['security.last_error']($request),
        'last_username' => $app['session']->get('_security.last_username'),

    ));
})->bind('login');

//vista dashboard general
$app->get('admin', function () use ($app) {
   $token = $app['security.token_storage']->getToken();
 //  echo "<font color='#7FFF00'>soy el $token </font>";
 $freespace=disk_free_space("/");

 $totalspace = disk_total_space("/");
                     $freespace_mb = $freespace/1024/1024/1024;
                     $totalspace_mb = $totalspace/1024/1024/1024;

$usado=round($totalspace_mb,2)-round($freespace_mb,2);

        return  $app['twig']->render('BackEnd/Dashboard.twig', array(
        'user' => $token->getUsername(),
        'total_libre'=> round($freespace_mb,2),
        'total_disco'=>round($totalspace_mb,2),
        'total_usado'=>$usado
    )
);
})->bind('vistaadmin')->before('Server\RegistroControler\RegistroControler::enviar_notificacion');
//;
//->before('Server\RegistroControler\RegistroControler::desde_aqui');





 $app->get('nombrearchivos', function (Request $request) use ($app) {
   $sel= $request->get("select");
   $name=$request->get("nombre");

  $sql=" select * from servidores where archivo_acceso= '".$name."' and tipo = '" .$sel ."'";
  $stmt=$app['db']->prepare($sql);
  $stmt->execute();
  $n=$stmt->fetchAll(PDO::FETCH_OBJ);
return $app->json(array("success"=>true,'data'=>$n),202);
})->bind('nombrearchivos');






$app->get('/graficar',function (Request $request) use ($app) {
  /*
  select count(tipo) as total ,fecha_registro from registros where tipo="tomcat"    and fecha_registro BETWEEN DATE("2017-10-10" )
   and DATE ("2017-10-20") group by fecha_registro;
  $fechauno= $request->get("fechauno");
  $fechados=$request->get("fechados");
  $tipo=$request->get("tipo");
  "select * from registros where tipo='.'mysql_error"."and fecha_registro BETWEEN DATE(\'".'2017-10-10'.'\' ) and DATE(\''.'2017-10-19'.'\')';
  */
  $fechados=$request->get("fechados");
  $fechauno= $request->get("fechauno");
  $tipo=$request->get("tipo");
  $sql="select count(tipo) as total ,fecha_registro from registros where tipo=\"".$tipo."\" and fecha_registro BETWEEN DATE(\"".$fechauno."\") and DATE(\"".$fechados."\") group by fecha_registro";
//$sql=" select * from registros where  tipo= \"mysql_error\" and fecha_registro = DATE(\" 2017-10-18 \") ";
 $stmt=$app['db']->prepare($sql);
 $stmt->execute();
 $n=$stmt->fetchAll(PDO::FETCH_OBJ);
return $app->json(array("success"=>true,'data'=>$n),202);
}
)->bind('graficar');





//vista para ver sertvidores
/*
$app->get('/verservidores', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/Vistas/verservidores.twig', array(
        'user'=>$app['session']->get('user')
      ));
 })->bind('verservidores');
*/
//$app->get('/versaludo','Server\RegistroControler\RegistroControler::desde_aqui')->bind('ruta');

$app->get('/verservidores','Server\ServerControler\ServerControler::listar')->bind('verservidores');
//vista para agregar servidores

 $app->get('/agregarservidores', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/Vistas/addserver.twig', array(
        'user'=>$token->getUsername()
      ));
 })->bind('agregarservidores');


//envio de los parametros al formulario
 $app->post('/envioformulario/addserver','Server\ServerControler\ServerControler::agregar')->bind('envioformularioservidor');
 //eliminar servidor
$app->match('/eliminarservidor/{id}','Server\ServerControler\ServerControler::eliminar')->bind('eliminarservidor');
$app->match('/actualizarservidor/{id}','Server\ServerControler\ServerControler::actualizar')->bind('actualizarservidor');
$app->match('/updateserver/{id}/','Server\ServerControler\ServerControler::updateserver')->bind('updateserver');
//vista para  eliminar servidores
/*
 $app->post('/eliminarservidores', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/Vistas/deleteserver.twig', array(
        'user'=>$app['session']->get('user')
      ));
 })->bind('eliminarservidores');
*/
/*
$app->post('/eliminarservidores','Server\ServerControler\ServerControler::eliminar')->bind('eliminarservidores');

*/
//vista para ver registros(solo los ultimos)
$app->get('/verregistros', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
    //arreglo que contiene el host de Elasticsearch
    $hosts = [
        '172.17.0.245:9200'//IP + Puerto
    ];
    $client = ClientBuilder::create() // Instancia de cliente que hara la consulta
->setHosts($hosts)  ->build();// Le mandamos el parametro $host para que construya la aplicacion

    //consulta de log mysql_error
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" : "mysql_error"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'mysql_error',
        'body' => $json];
    $results_mysqlerror = $client->search($params);
    //nueva consulta log apache_error
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" : "apache_error"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'apache_error',
        'body' => $json];
    $result_apacheerror=$client->search($params);
    //nuevaconsulta de log propio
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" : "logpropio"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'logpropio',
        'body' => $json];
    $results_logpropio=$client->search($params);
    //consulta para apache_access
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" : "apache_access"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'acceso',
        'body' => $json];
    $result_apacheacees=$client->search($params);
    //consulta tomncat_access
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" :"tomcat"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'tomcat_acess',
        'body' => $json];
    $result_tomcatacees=$client->search($params);

    //consulta catalina_error
    $json = '{
        "query" : {
            "bool" : {
                "filter" : {
                    "term" : {
                        "type" : "tomcat_out"
                    }
                }
            }
        },
        "sort" : {
            "@timestamp": {"order": "desc"}
        }
    }';
    $params = [
        'index' => 'tomcat_out',
        'body' => $json];
    $result_catalina=$client->search($params);
    //var_dump($results_mysqlerror);
    //exit;
     return  $app['twig']->render('BackEnd/Vistas/verregistros.twig', array(
        'user'=>$app['session']->get('user'),
        'data' =>$results_mysqlerror,//["hits"]{"hits"}[0]["_type"]//le mandamos el parametro
        'data2'=>$result_apacheerror,
        'data3'=>$results_logpropio,
        'data4'=>$result_apacheacees,
        'data5'=>$result_tomcatacees,
        'data6'=>$result_catalina

      ));
 })->bind('verregistros');

//vista para ver notificaciones
$app->get('/vernotificaciones', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/Vistas/vernotificaciones.twig', array(
        'user'=>$app['session']->get('user')
      ));
 })->bind('vernotificaciones');

 //vista para configuracion de notificaciones
 $app->get('/confnotificaciones', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/Vistas/confnotificaciones.twig', array(
        'user'=>$app['session']->get('user')
      ));
 })->bind('confnotificaciones');

//vista para las estadisticas
$app->get('/chart', function () use ($app) {
    $token = $app['security.token_storage']->getToken();
     return  $app['twig']->render('BackEnd/chart.twig', array(
        'user'=>$app['session']->get('user')
      ));
 })->bind('chart');


/*funcion para crear bd*/
$app->get('/crear', function () use ($app) {
   $schema = $app['db']->getSchemaManager();
    //if (!$schema->tablesExist('users')) {
        //$users = new Table('users');
        //$users->addColumn('id', 'integer', array('unsigned' => true, 'autoincrement' => true));
        //$users->setPrimaryKey(array('id'));
        //$users->addColumn('username', 'string', array('length' => 32));
        //$users->addUniqueIndex(array('username'));
        //$users->addColumn('password', 'string', array('length' => 255));
        //$users->addColumn('roles', 'string', array('length' => 255));

        //$schema->createTable($users);
        //password = foo
        $app['db']->executeQuery('INSERT INTO users (username, password, roles) VALUES ("admin2", "gg", "ROLE_ADMIN")');
        $app['db']->executeQuery('INSERT INTO users (username, password, roles) VALUES ("juanlu2", "hh", "ROLE_USER")');
    //}

    return "Creada";
})->bind('bd');
