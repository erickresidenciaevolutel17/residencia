<?php

namespace auntentificacion;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationSuccessHandler as BaseDefaultAuthenticationFailureHandler;
use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use \Symfony\Component\HttpFoundation\RedirectResponse;
use Silex\Provider\SecurityServiceProvider;
class DefaultAuthenticationSuccessHandler extends BaseDefaultAuthenticationFailureHandler {

    public $app;

    public function __construct(Application $app) {
        $this->app = $app;
    }
    public function redirectToHome(Request $request, $user, TokenInterface $token = null) {
        $app = $this->app;
        $roles = $app['session']->get('user')->getRoles();
        if (in_array('ROLE_ADMIN', $roles)) {
            $response = new RedirectResponse($app['url_generator']->generate('home_admin'));
        } else if (in_array('ROLE_EMPRESA', $roles)) {
            $response = new RedirectResponse($app['url_generator']->generate('home_empresa'));
        } else if (in_array('ROLE_REGISTRO', $roles)) {
            $response = new RedirectResponse($app['url_generator']->generate('home_registro'));
        } else if (in_array('ROLE_AGENTE', $roles)) {
            $response = new RedirectResponse($app['url_generator']->generate('home_agente'));
        } else {
            $response = new RedirectResponse($app['url_generator']->generate('home'));
        }
        return $response;
    }
    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {
        $app = $this->app;
        $user =$app['security.token_storage']->getToken()->getUser();
        $app['session']->set('user', $user);
        if ($app['security.authorization_checker']->isGranted('ROLE_ADMIN')){
            return new RedirectResponse($app['url_generator']->generate('vistaadmin'));
        }


        return $response;
    }

}