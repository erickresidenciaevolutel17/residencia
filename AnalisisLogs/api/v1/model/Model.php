<?php

use Silex\Application as Application;

abstract class Model {

 protected $tabla;
 protected $id_tabla;

 public function __construct(Application $app) {
 $this->app = $app;
 }

 abstract protected function getColumsDefault($colums = "*");

 public function insert_id($data){
 $this->app['db']->beginTransaction();
 try{
 $this->app['db']->insert($this->tabla, (array) $data);
 $id = $this->app['db']->lastInsertId();
 $this->app['db']->commit();
 return $id;
 }catch(Exception $e) {
 $this->app['db']->rollBack();
 return null;
 }
 }

 public function insert($data) {
 $campos = "";
 $params = "";
 foreach ($data as $key => $value) {
 $campos.="$key,";
 $params.=":$key,";
 }
 $campos = substr($campos, 0, strlen($campos) - 1);
 $params = substr($params, 0, strlen($params) - 1);
 $query = "insert into $this->tabla ($campos) values ($params)";
 $statement = $this->app['db']->prepare($query);
 return $statement->execute($data);
 }

 public function delete($id) {
 $sql = "DELETE FROM $this->tabla WHERE $this->id_tabla = :id";
 $stmt = $this->app['db']->prepare($sql);
 $stmt->bindParam(':id', $id, PDO::PARAM_STR);
 return $stmt->execute();
 }

 public function update($id, $data) {
 $set = "";
 if (is_array($data)) {
 foreach ($data as $key => $value) {
 $set.=" $key=:$key ,";
 }
 $set = substr($set, 0, strlen($set) - 1);
 $sql = "UPDATE $this->tabla set $set WHERE $this->id_tabla = :id ;";
 $stmt = $this->app['db']->prepare($sql);
 try {
 $params = array(":id" => $id);
 foreach ($data as $key => $value) {
 $params[":$key"] = $value;
 }
 $stmt->execute($params);
 return True;
 } catch (Exception $e) {
 var_dump($e->getMessage());
 }
 }
 return False;
 }

 public function last_id() {
 $id = $this->app["db"]->lastInsertId();
 return $id;
 }

 public function getTotal() {
 $total = $this->app['db']->fetchAll("SELECT count(*) as total FROM $this->tabla ")[0];
 return intval($total["total"]);
 }

 public function getAll($colums = "*") {
 $colums = $this->getColumsDefault($colums);
 $total = $this->app['db']->fetchAll("SELECT $colums FROM $this->tabla ");
 return $total;
 }

 public function getList($pagina, $colums = "*") {
 $colums = $this->getColumsDefault($colums);
 $total = $this->app['db']->fetchAll("SELECT $colums FROM $this->tabla " . $this->limit($pagina));
 return $total;
 }

 public function getListWhere($where, $colums = "*", $order_by = "") {
 $colums = $this->getColumsDefault($colums);
 $total = $this->app['db']->fetchAll("SELECT $colums FROM $this->tabla where $where $order_by");
 return $total;
 }

 protected function query($query,$params=array(),$obj=false) {
     $stmt = $this->app['db']->prepare($query);
        $stmt->execute($params);
        if($obj){
        $tipos = $stmt->fetchAll(PDO::FETCH_OBJ);
        }else{

        $tipos = $stmt->fetchAll();
        }
 return $tipos;
 }

 public function get($id, $colums = "*") {
 $colums = $this->getColumsDefault($colums);
 if (is_array($id)) {
 $where = "";
 $c = 0;
 foreach ($id as $key => $value) {
 if (count($id) - 1 == $c) {
 $where.=" $key='$value' ";
 } else {
 $where.=" $key='$value' and";
 }
 $c++;
 }
 //$campos = substr($campos, 0, strlen($campos) - 1);
 $sql = "SELECT $colums FROM $this->tabla where $where limit 1 ";
 } else {
 $sql = "SELECT $colums FROM $this->tabla where $this->id_tabla=? limit 1";
 }
 $n = $this->app['db']->fetchAll($sql);
  $aux;
  if(count($n)>0){
return $n[0];

  }

 }


 public function getCount($where) {
 $total = $this->app["db"]->fetchAll("SELECT count(*) as total FROM $this->tabla where $where ")[0];
 return $total["total"];
 }

 protected function limit($pag) {
 if (!$pag || $pag == 0) {
 return "limit 15";
 } else {
 return "limit " . ($pag * 15) . ",15";
 }
 }

}
