<?php

use Silex\Application as Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;

$app->post('/usuario/login', function( Application $app, Request $request) {
    try {
        $userDB = new Usuario($app);
        $usuario = $userDB->get(array(
            "username" => $request->get("usuario"),
            "password" => sha1($request->get("password"))
        ));
        if ($usuario && count($usuario) > 0) {
            $usuario["token"] = Token::generate($usuario["username"], $usuario["password"]);
            unset($usuario["password"]);
            //obtener request el token de firebase y guardarlo junto con el id de usuario
            return $app->json(array("success" => true, "data" => $usuario), 202);
        }

    }
    catch (Exception $e) {

    }
    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});




$app->get('/ggg', function( Application $app, Request $request) {
    return $app->json(array("success" => "hola", "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});

$app->get('/notificaciones', function( Application $app, Request $request) {
  $sql="select id_registro,mensaje,fecha_registro,tipo from registros  order by id_registro desc limit 10";
  $stmt=$app['db']->prepare($sql);
  $stmt->execute();
  $n=$stmt->fetchAll(PDO::FETCH_OBJ);
  return $app->json(array("success"=>true,'data'=>$n),202);
  //  return $app->json(array("success" => "hola", "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);
});


  $app->get('/conexion', function( Application $app, Request $request) {
   try {
        $userDB = new Usuario($app);
        $usuario = $userDB->get(array(
            "username" => "erick garcia",
            "password" => sha1("Camaleon")
        ));
        if ($usuario && count($usuario) > 0) {
           $usuario["token"] = Token::generate($usuario["username"], $usuario["password"]);
            unset($usuario["password"]);
            return $app->json(array("success" => true, "data" => $usuario), 202);
        }
    }
   catch (Exception $e) {

  }
    return $app->json(array("success" => false, "status_code" => 1, "mensaje" => "Datos no encontrados"), 202);

});
