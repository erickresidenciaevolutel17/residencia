<?php
//Controlador frontal
require_once __DIR__.'/vendor/autoload.php';
use Silex\Application;
use Symfony\Component\HttpFoundation\Request as Request;
use Symfony\Component\HttpFoundation\Response as Response;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;


$app = new Application();
$app->register(new Silex\Provider\ValidatorServiceProvider());

# CONFIGURACION //preguntar
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^.*$',
            'http' => true,
            'anonymous' => false,
            'users' => array(
                  'admin' => array('ROLE_ADMIN', 'e964b65ef74b986ec46382cb2cbe1758e91f963a'), //Camaleon
                'mobile' => array('ROLE_ADMIN', '9f047a8af6c607ca5cb24120890fe4e8f523c89d'), //mobile-pass-2016
        ),
        ),
    )
));
$app['security.default_encoder'] = function ($app) {
    // Plain text (e.g. for debugging)
    return new MessageDigestPasswordEncoder('sha1',false,1);
};
$app->register(new Silex\Provider\ValidatorServiceProvider());
#Configuracion Base de Datos.
$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'host' => '172.17.0.245',
        'dbname' => 'users',
        'user' => 'clusterelastic@172.17.0.245',
        'password' => 'Camaleon',
        'charset' => 'utf8'
    )
));
# GESTION DE ERRORES
$app->error(function (\Exception $exception, $code) use ($app) {

    return $app->json(array('success' => 0, 'data' => array('success' => false, 'data' =>
                    array(
                        'file' => $exception->getFile(),
                        'line' => $exception->getLine(),
                        'message' => $exception->getMessage(),
                        'trace' => $exception->getTraceAsString(),
                        'code' => $code
                    )
                )), 403);
});

include_once './library/Token.php';
include_once './model/Model.php';
include_once './model/Usuario.php';
include_once './rutas/usuario.php';

$app->boot();
$app->run();
