<?php
//Controlador frontal
require_once __DIR__.'/vendor/autoload.php';

$app = require __DIR__.'/src/app.php';
include_once './config.php';
require __DIR__.'/src/controler/controllers.php';
require   __DIR__.'/src/controler/AuthSuccessHandler.php';
require   __DIR__.'/src/controler/servercontroler.php';
require   __DIR__.'/src/controler/registroscontroler.php';
include_once './src/models/Model.php';
include_once './src/models/Servidores.php';
include_once './src/models/Registros.php';

$app->boot();
$app->run();
//$app->boot();
