﻿using System;
using System.Net.Http;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LogAnalyzer3.api.models;

using LogAnalizer3;

namespace LogAnalyzer3
{
	public class AJAX<T>
	{

		string user;
		string pass;
		HttpClient client;

		public AJAX()
		{
			user = Config.USER;
			pass = Config.PASS;
			var authData = string.Format("{0}:{1}", this.user, this.pass);
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
			client = new HttpClient();
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
		}


		public async Task<Respuesta<T>> Get(string url, List<KeyValuePair<string, string>> pairs = null)
		{
			var Items = new Respuesta<T>();
			var uri = new Uri(Config.SERVER + url + parametrosGET(pairs));
			try
			{
				var response = await client.GetAsync(uri);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					Debug.WriteLine(content);
					Items = JsonConvert.DeserializeObject<Respuesta<T>>(content);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"ERROR {0}", ex.Message);
			}
			return Items;
		}


		public async Task<Respuesta<List<T>>> GetList(string url, List<KeyValuePair<string, string>> pairs = null)
		{
			var Items = new Respuesta<List<T>>();
			Debug.WriteLine(Config.SERVER + url + parametrosGET(pairs));
			var uri = new Uri(Config.SERVER + url + parametrosGET(pairs));
			try
			{
				var response = await client.GetAsync(uri);

				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					Debug.WriteLine(content);
					Items = JsonConvert.DeserializeObject<Respuesta<List<T>>>(content);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"ERROR {0}", ex.Message + " de aqui viene");
			}
			return Items;
		}

		private string parametrosGET(List<KeyValuePair<string, string>> pairs = null)
		{
			if (pairs != null)
			{
				string res = "?";
				for (var i = 0; i < pairs.Count; i++)
				{
					res += pairs[i].Key + "=" + pairs[i].Value + "&";
				}
				return res;
			}
			return "";
		}


		public async Task<Respuesta<T>> Post(string url, List<KeyValuePair<string, string>> pairs)
		{
			var Items = new Respuesta<T>();
			var uri = new Uri(Config.SERVER + url);
			Debug.WriteLine(Config.SERVER + url + pairs[0]+" url post");
			try
			{
				var content2 = new FormUrlEncodedContent(pairs);
				var response = await client.PostAsync(uri, content2);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();
					Debug.WriteLine(content +" aqui esta el resultado");

					Items = JsonConvert.DeserializeObject<Respuesta<T>>(content);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"ERROR {0}", ex.Message );
			}
			return Items;
		}

		public async Task<List<T>> PostList(string url, List<KeyValuePair<string, string>> pairs)
		{
			var Items = new List<T>();
			var uri = new Uri(Config.SERVER + url);
			try
			{
				var content2 = new FormUrlEncodedContent(pairs);
				var response = await client.PostAsync(uri, content2);
				if (response.IsSuccessStatusCode)
				{
					var content = await response.Content.ReadAsStringAsync();

					Items = JsonConvert.DeserializeObject<List<T>>(content);
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine(@"ERROR {0}", ex.Message);
			}
			return Items;
		}

	}
}