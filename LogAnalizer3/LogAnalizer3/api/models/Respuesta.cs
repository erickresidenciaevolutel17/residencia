﻿using System;
namespace LogAnalyzer3.api.models

{
	public class Respuesta<T>
	{


		private bool success;
		private T data;


		public Respuesta()
		{
		}


		public bool Success
		{
			get
			{
				return success;
			}

			set
			{
				success = value;
			}
		}

		public T Data
		{
			get
			{
				return data;
			}

			set
			{
				data = value;
			}
		}

		public string Mensaje
		{
			get;
			set;
		}

	}
}

