﻿using System;
using SQLite.Net.Attributes;
namespace LogAnalyzer3.api.models
{
	public class User
	{

		private string usuario;
		private string password;
		private string nombre;
		private string token;
		private string rol;


		public User()
		{


		}

		public User(string token)
		{
			this.token = token;
		}

		[PrimaryKey]
		public string Token
		{
			get;
			set;
		}


		public string Usuario
		{
			get;
			set;
		}

		public string Password
		{
			get;
			set;
		}


		public string Nombre
		{
			get;

			set;
		}

		public string Rol
		{
			get;

			set;
		}


		public override string ToString()
		{
			//return string.Format("[User: token={0}, nombre={1}, apellido={2}, correo={3}, rol={4}]", token, nombre, apellido, correo, rol);
			return string.Format("[User: token={0}, nombre={1},rol={2}]", token, nombre, rol);
		}

	}
}