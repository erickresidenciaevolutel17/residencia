﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using SQLite.Net;
using System.Diagnostics;
using Xamarin.Forms;
using LogAnalyzer3.api.models;
using LogAnalyzer3.api.db;


namespace LogAnalyzer3
{

	public class LogAnalyzer
	{

		private User usuario;

		SQLiteConnection database;

		public LogAnalyzer()
		{
		}

		public bool isLogin()
		{
			if (this.usuario != null)
			{
				return true;
			}
			using (var conn = DependencyService.Get<ISQLite>().GetConnection())
			{
				conn.CreateTable<User>();
				var user = conn.Query<User>("select * from user limit 1");
				conn.Close();
				if (user.Count != 0)
				{
					usuario = user[0];
					return true;
				}
			}
			return false;
		}

		public User getUser()
		{
			if (this.usuario != null)
			{
				return this.usuario;
			}
			using (var conn = DependencyService.Get<ISQLite>().GetConnection())
			{
				conn.CreateTable<User>();
				var user = conn.Query<User>("select * from user limit 1");
				conn.Close();
				if (user.Count != 0)
				{
					usuario = user[0];
					return user[0];
				}
			}
			return null;
		}


		public void Logout()
		{
			this.usuario = null;
			using (var conn = DependencyService.Get<ISQLite>().GetConnection())
			{
				conn.DropTable<User>();

			}
		}

		public async Task<User> Login(string user, string pass)
		{

			if (!isLogin())
			{
				var pairs = new List<KeyValuePair<string, string>>
				{
					new KeyValuePair<string, string>("usuario", user),
					new KeyValuePair<string, string>("password", pass)
				};
				var ajax = new AJAX<User>();
				Respuesta<User> resp = await ajax.Post("/usuario/login", pairs);

				if (resp.Success)
				{
					var sql = DependencyService.Get<ISQLite>();
					database = sql.GetConnection();
					using (var conn = database)
					{
						conn.Insert(resp.Data);
					}
					this.usuario = resp.Data;
					return resp.Data;
				}
			}
			else
			{
				using (var conn = DependencyService.Get<ISQLite>().GetConnection())
				{
					var res = conn.Query<User>("select * from user limit 1");
					if (res.Count != 0)
					{
						conn.Close();
						this.usuario = res[0];
						return res[0];
					}
				}
			}
			return null;
		}

	}
}

