﻿using System;
using SQLite.Net;
namespace LogAnalyzer3.api.db
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}