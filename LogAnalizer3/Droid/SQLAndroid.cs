﻿using System;

using System.IO;
using Xamarin.Forms;
using LogAnalyzer3.api.db;


[assembly: Dependency(typeof(LogAnalyzer3.api.db.SQLite_Android))]

namespace LogAnalyzer3.api.db
{
	public class SQLite_Android : ISQLite
	{
		public SQLite_Android()
		{
		}


		public SQLite.Net.SQLiteConnection GetConnection()
		{
			var fileName = "clubnupcial.db3";
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			var path = Path.Combine(documentsPath, fileName);

			var platform = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
			var connection = new SQLite.Net.SQLiteConnection(platform, path);
			return connection;
		}

	}
}